# Kicad Library

## attension
{$KIKAIKEN_DIR}をクローンしたディレクトリまでのパスとしてKicadに登録すること。

## Classification rules

~~各symbol(回路図記号)は部品名・型番名で登録。用途ごとに種類分け
各footprint,3Dpackageは **部品型番よりも 、パッケージ型番を優先させます。**
footprint,3Dpackageともに、種類分けを行った.lib .prettyに保管~~
`現在調整中`

## Contents
`今後symbol,footprint,3Dpackageフォルダに分類予定`

STM_Nucleo

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|マイコンボード|NUCLEO-F767ZI|Nucleo-F767ZI_Morpho|Nucleo-F767ZI_Morpho|(デフォのピンソケットを参照)|-|
|マイコンボード|NUCLEO-F767ZI|Nucleo-F767ZI_Morpho|Nucleo-F767_with_power_selection|(デフォのピンソケットを参照)|電源入力選択のジャンパ付き|
|マイコンボード|NUCLEO-F303k8|-|-|(デフォのピンソケットを参照)|
|マイコンボード|NUCLEO-F446RE|-|-|-|

aki_Switch

|種類|品名|シンボル|フットプリント|3Ddata|備考|URL|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|:--|
|スイッチ|42J02GB|42J02GB|-|-|
|スイッチ|MP86A1GN3H-G|MP86A1GN3H-G|-|-|
|スイッチ|SKRPACE010|SKRPACE010|-|SKRPAC.STEP|
|スイッチ|ST12-303FCG|ST12-303FCG|-|-|
|スイッチ|(defaultを使用)|-|4MS1R202M6QES|-|http://akizukidenshi.com/catalog/g/gP-07439/|

aki_Fuse

|種類|品名|シンボル|フットプリント|3Ddata|備考|URL|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|:--|
|ヒューズ|-|-|-|-|

aki_Heatsink

|種類|品名|シンボル|フットプリント|3Ddata|備考|URL|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|:--|
|ヒートシンク|-|-|-|-|

Connector_JST_XA

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|コネクタ|XA 2pin Vertical|-|JST_XA_B02B-XASK-1_1x02_P2.50mm_Vertical.kicad_mod|B02B-XASK-1.STEP|
|コネクタ|XA 3pin Vertical|-|JST_XA_B03B-XASK-1_1x03_P2.50mm_Vertical.kicad_mod|B03B-XASK-1.STEP|
|コネクタ|XA 4pin Vertical|-|JST_XA_B04B-XASK-1_1x04_P2.50mm_Vertical.kicad_mod|B04B-XASK-1.STEP|
|コネクタ|XA 5pin Vertical|-|JST_XA_B05B-XASK-1_1x05_P2.50mm_Vertical.kicad_mod|B05B-XASK-1.STEP|
|コネクタ|XA 6pin Vertical|-|JST_XA_B06B-XASK-1_1x06_P2.50mm_Vertical.kicad_mod|B06B-XASK-1.STEP|
|コネクタ|XA 2pin Horizontal|-|JST_XA_S02B-XASK-1_1x02_P2.50mm_Horizontal.kicad_mod|S02B-XASS-1.STEP|
|コネクタ|XA 3pin Horizontal|-|JST_XA_S03B-XASK-1_1x03_P2.50mm_Horizontal.kicad_mod|S03B-XASK-1.STEP|
|コネクタ|XA 4pin Horizontal|-|JST_XA_S04B-XASK-1_1x04_P2.50mm_Horizontal.kicad_mod|S04B-XASK-1.STEP|
|コネクタ|XA 5pin Horizontal|-|JST_XA_S05B-XASK-1_1x05_P2.50mm_Horizontal.kicad_mod|-|
|コネクタ|XA 6pin Horizontal|-|JST_XA_S06B-XASK-1_1x06_P2.50mm_Horizontal.kicad_mod|S06B-XASK-1.STEP|

Connector_JST_VH

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|コネクタ|VH 2pin Vertical|(デフォルトのcon_generalを使用)|(デフォルトを使用)|JST-VH-B2P.stp|
|コネクタ|VH 3pin Vertical|-|-|JST-VH-B3P.stp|
|コネクタ|VH 4pin Vertical|-|-|JST-VH-B4P.stp|
|コネクタ|VH 5pin Vertical|-|-|JST-VH-B5P.stp|
|コネクタ|VH 6pin Vertical|-|-|JST-VH-B6P.stp|
|コネクタ|VH 7pin Vertical|-|-|JST-VH-B7P.stp|
|コネクタ|VH 8pin Vertical|-|-|JST-VH-B8P.stp|
|コネクタ|VH 9pin Vertical|-|-|JST-VH-B9P.stp|
|コネクタ|VH 10pin Vertical|-|-|JST-VH-B10P.stp|
|コネクタ|VH 11pin Vertical|-|-|JST-VH-B11P.stp|
|コネクタ|VH 12pin Vertical|-|-|JST-VH-B12P.stp|
|コネクタ|VH 2pin Horizontal|-|-|JST-VH-B2PS.STEP|サイドカバーの無いタイプ|
|コネクタ|VH 2pin Horizontal|-|-|JST-VH-S2P.STEP|サイドかパーのあるタイプ|

Connector_AMASS

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|コネクタ||-|-|-|

gyro

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|IMU|-|-|-|-|ブレイクアウト|

Jumper

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|半田ジャンパ|-|-|-|-|

DCDC

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|DCDC|-|-|-|-|

Communication_device

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|CAN IC|-|-|-|-|
|RS485 IC|-|-|-|-|
|フォトカプラ|-|-|-|-|

Power_Device

|種類|品名|シンボル|フットプリント|3Ddata|備考|
|:-----------|:------------|:-------------------|:--------------------|:-------------|:---|
|モータドライバ|-|-|-|-|
